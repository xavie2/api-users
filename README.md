## API Users

Primero debe instalar los paquetes de dependencias del proyecto con el comando:

```
npm install
```

Configurar los datos de configuración en el archivo de configuración .env

```
DB_USER="postgres"
DB_HOST="localhost"
DB_NAME="db_modulo3"
DB_PASSWORD="postgres"
DB_PORT="5433"
```

Para iniciar el servicio de la API se debe ejecutar el siguiente comando en la raiz del proyecto:

```
node ./src/app.js
```

Crear la base de datos llamada `db_modulo3` y ejecutar el script del archivo `table-users.sql` que se encuentra en la raiz del proyecto

```
-- Creacion de la tabla users
CREATE TABLE users ( 
	id SERIAL PRIMARY KEY,
	cedula_identidad VARCHAR(10) UNIQUE,
	nombres VARCHAR(255) NOT NULL,
	primer_apellido VARCHAR(50) NOT NULL,
	segundo_apellido VARCHAR(50),
	fecha_nacimiento DATE NOT NULL 
); 

-- Insertar datos en la tabla de users
INSERT INTO users (cedula_identidad, nombres, primer_apellido, segundo_apellido, fecha_nacimiento)
VALUES 
  ('1234567', 'Juan', 'Pérez', null, '1987-12-03'),
  ('1234568', 'Daniel', 'Fernandez', 'Rios', '1990-10-23'),
  ('1234569', 'Pablo', 'Duran', 'Flores', '1993-06-12');
```

## Proyecto desplegado en render.com

[https://api-users-modulo-3.onrender.com/](https://api-users-modulo-3.onrender.com/)

## Documentación de la API

GET `/`	Referencia de la API

GET `/estado`	Información General  y version de la API

POST `/usuarios`	Servicio para crear un usuario

    Parámetros

| Parámetro      | Tipo              |
| --------------- | ----------------- |
| cedulaIdentidad | string            |
| nombres         | string            |
| primerApellido  | string            |
| segundoApellido | string            |
| fechaNacimiento | date (YYYY/mm/dd) |

Body Json

```
{
    "cedulaIdentidad": "5574776",
    "nombres": "Franz Javier",
    "primerApellido": "Muraña",
    "segundoApellido": "Cruz",
    "fechaNacimiento": "1989-10-20"
}
```

GET `/usuarios`	Servicio para obtener la lista de usuarios

GET `/usuarios/:id_usuario`	Servicio para obtener los datos de un determinado usuario

PUT `/usuarios/:id_usuario`	Servicio para actualizar los datos de un determinado usuario

Parámetros

| Parámetro      | Tipo              |
| --------------- | ----------------- |
| cedulaIdentidad | string            |
| nombres         | string            |
| primerApellido  | string            |
| segundoApellido | string            |
| fechaNacimiento | date (YYYY/mm/dd) |

Body Json

```
{
    "cedulaIdentidad": "5574776",
    "nombres": "Franz Javier",
    "primerApellido": "Muraña",
    "segundoApellido": "Cruz",
    "fechaNacimiento": "1989-10-20"
}
```

DELETE `/usuarios/:id_usuario` Servicio para eliminar un determinado usuario

GET `/promedio-edad`	Servicio para obtener el promedio de edades de los usuarios

## Documentación en línea - POSTMAN

[https://documenter.getpostman.com/view/23799312/2s93m354ET](https://documenter.getpostman.com/view/23799312/2s93m354ET)
