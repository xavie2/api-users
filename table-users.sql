﻿-- Creacion de la tabla users
CREATE TABLE users ( 
	id SERIAL PRIMARY KEY,
	cedula_identidad VARCHAR(10) UNIQUE,
	nombres VARCHAR(255) NOT NULL,
	primer_apellido VARCHAR(50) NOT NULL,
	segundo_apellido VARCHAR(50),
	fecha_nacimiento DATE NOT NULL 
); 

-- Insertar datos en la tabla de users
INSERT INTO users (cedula_identidad, nombres, primer_apellido, segundo_apellido, fecha_nacimiento)
VALUES 
  ('1234567', 'Juan', 'Pérez', null, '1987-12-03'),
  ('1234568', 'Daniel', 'Fernandez', 'Rios', '1990-10-23'),
  ('1234569', 'Pablo', 'Duran', 'Flores', '1993-06-12');

