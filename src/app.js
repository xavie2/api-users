const express = require('express');
const { Pool } = require('pg');

require('dotenv').config();

const Config = require('./model/configModel');
const ConfigController = require('./controller/configController');

const User = require('./model/userModel');
const UserController = require('./controller/userController');

const app = express();
const port = 3001;

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
});

app.use(express.json());

// Instanciacion
const configModel = new Config();
const configController = new ConfigController(configModel);

const userModel = new User(pool);
const userController = new UserController(userModel);

// Routes
app.get('/', configController.getConfig.bind(configController));
app.get('/estado', configController.getEstado.bind(configController));

app.post('/usuarios', userController.addUser.bind(userController));
app.get('/usuarios', userController.getUsers.bind(userController));
app.get('/usuarios/:id_usuario', userController.getUserById.bind(userController));
app.put('/usuarios/:id_usuario', userController.updateUser.bind(userController));
app.delete('/usuarios/:id_usuario', userController.deleteUser.bind(userController));

app.get('/promedio-edad', userController.getAvgAge.bind(userController));

app.listen(port, () => {
    console.log(`Servidor iniciado en http://localhost:${port}`);
});