class UserController{
    constructor(model) {
        this.model = model;
    }

    async addUser(req, res) {
        const cedulaIdentidad = req.body.cedulaIdentidad;
        const nombres = req.body.nombres;
        const primerApellido = req.body.primerApellido;
        const segundoApellido = req.body.segundoApellido;
        const fechaNacimiento = req.body.fechaNacimiento;

        await this.model.addUser(cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento);
        res.sendStatus(201);
    }

    async getUsers(req, res) {
        const data = await this.model.getUsers();
        res.send(data);
    }

    async getUserById(req, res) {
        const id = req.params.id_usuario;
        const data = await this.model.getUserById(id);
        res.send(data);
    }

    async updateUser(req, res) {
        const id = req.params.id_usuario;
        const cedulaIdentidad = req.body.cedulaIdentidad;
        const nombres = req.body.nombres;
        const primerApellido = req.body.primerApellido;
        const segundoApellido = req.body.segundoApellido;
        const fechaNacimiento = req.body.fechaNacimiento;

        await this.model.updateUser(id, cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento);
        res.sendStatus(200);
    }

    async deleteUser(req, res) {
        const id = req.params.id_usuario;
        await this.model.deleteUser(id);
        res.sendStatus(200);
    }

    async getAvgAge(req, res) {        
        const data = await this.model.getAvgAge();
        res.send(data);
    }
}

module.exports = UserController;