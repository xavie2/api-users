class ConfigController{
    constructor(model) {
        this.model = model;
    }

    getConfig(req, res) {        
        const data = this.model.getConfig();
        res.send(data);
    }
    getEstado(req, res) {        
        const data = this.model.getEstado();
        res.send(data);
    }
}

module.exports = ConfigController;