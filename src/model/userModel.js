class User {
    constructor(pool) {
        this.pool = pool;
    }

    async addUser(cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento) {
        await this.pool.query("INSERT INTO users (cedula_identidad, nombres, primer_apellido, segundo_apellido, fecha_nacimiento) values ($1, $2, $3, $4, $5)", [cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento]);
    }

    async getUsers() {
        const { rows } = await this.pool.query("select * from users;");
        return rows;
    }

    async getUserById(id) {
        const { rows } = await this.pool.query("select * from users where id = $1;", [
          id,
        ]);
        return rows[0];
    }

    async updateUser(id, cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento) {
        await this.pool.query("UPDATE users SET cedula_identidad=$1, nombres=$2, primer_apellido=$3, segundo_apellido=$4, fecha_nacimiento=$5 WHERE id=$6", [cedulaIdentidad, nombres, primerApellido, segundoApellido, fechaNacimiento, id]);
    }

    async deleteUser(id) {
        await this.pool.query("DELETE FROM users WHERE id=$1;", [id]);
    }

    async getAvgAge() {
        const { rows } = await this.pool.query("SELECT AVG(EXTRACT(YEAR FROM AGE(NOW(), fecha_nacimiento))) AS promedioEdad FROM users;");
        return rows[0];
    }
}
  
module.exports = User;