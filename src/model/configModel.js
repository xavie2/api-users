class Config {    
    getEstado() {
        const data = {
            nameSystem: 'api-users',
            version: '1.0.0',
            developer: 'Franz Javier Muraña Cruz',
            email: 'xavie2@gmail.com'
        };
        return data;
    }

    getConfig() {
        const data = {
            info: 'API Users - Trabajo Final del Módulo 3',                        
        };
        return data;
    }
}
  
module.exports = Config;